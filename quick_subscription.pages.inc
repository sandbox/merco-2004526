<?php
function subscribe_confirm_subscription($arg) {
if (!empty($arg)) {
$sid = subscribe_load_by_hash($arg);
  if ($sid) {
       if ($sid['status'] == 0) {
	   subscribe_activate_status($sid['sid']);
	   drupal_set_message(variable_get('subscription_confirm_message', 'Now you have completed your subscription. Thanks for your interest.') );

	   } else {
	    drupal_set_message('This e-mail address has allready been confirmed.', 'warning'); 
	   }
  } else {
      drupal_not_found();
  }
}

return l('<h2>Go to home page</h2>', '<front>', array('html' => TRUE));
}
function subscribe_cancel_subscription($arg) {
if (!empty($arg)) {
$subsciber = subscribe_load_by_hash($arg);
   if ($subsciber) {
  $sid = $subsciber['sid'];
	   if ($subsciber['status'] == 1) {
	     return drupal_get_form('subscription_cancel_form', $sid);
	   }  else { drupal_not_found(); }

  } else { drupal_not_found(); }

}

}

function subscription_cancel_form($form, &$form_state, $sid) {
$form = array();
$form['sure'] = array(
  '#markup' => '<div>Are You sure you want to unsubscribe from the mailing list ?</div>'
);
$form['sid'] = array('#type' => 'value', '#value' => $sid);
$form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Confirm unsubscription'),
  );
 $form['cancel'] = array(
  '#markup' => l('cancel', '<front>')
);
return $form;
}
function subscription_cancel_form_submit($form, &$form_state) {
subscribe_cancel_subscriber($form_state['values']['sid']);
$form_state['redirect'] = '<front>';
drupal_set_message( variable_get('subscription_cancel_message', 'You have been unsubscribed from the mailing list.'));
}
function subscribe_settings_page() {
return drupal_get_form('subscribe_settings_form');

}
function subscribe_settings_form($form, &$form_state) {
$message = 'Now you have completed your subscription. Thanks for your interest.';
$cancel = 'You have unsubscribed from the mailing list.';
 $form['subscription_confirm_message'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Subscription confirmation text'),
    '#description' => t('This text will be shown to users when they confirm their subscription.'),
    '#default_value' => variable_get('subscription_confirm_message', $message),
  );
$form['subscription_allow_unsubscribe'] = array(
'#type' =>'checkbox',
'#title' => t('Allow unsubscription'),
'#description' => t('Check this to allow users to be able to unsubscribe.'),
'#default_value' => variable_get('subscription_allow_unsubscribe', 1),
);
/*$form['subscription_include_users'] = array(
'#type' =>'checkbox',
'#title' => t('Include registered users'),
'#description' => t('If you check this, Registered users will be included to the mailing list.'),
'#default_value' => variable_get('subscription_include_users', 0),
); */
 $form['subscription_cancel_message'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Subscription cancel text'),
    '#description' => t('This text will be shown to users after they unsubscribe'),
    '#default_value' => variable_get('subscription_cancel_message', $cancel),
     '#states' => array(
    'visible' => array(
       ':input[name="subscription_allow_unsubscribe"]' => array('checked' => TRUE),
      ),
    ),
  );
$template_raw = variable_get('subscription_message_template', '');
$template = $template_raw['value'];
 $form['subscription_message_template'] = array(
    '#type' => 'text_format',
	'#format' => 'full_html',
    '#title' => t('Message Template'),
    '#default_value' => $template
  );

return system_settings_form($form);

}